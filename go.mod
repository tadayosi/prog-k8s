module bitbucket.org/tadayosi/prog-k8s

go 1.13

require (
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/stretchr/testify v1.6.1
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	golang.org/x/time v0.0.0-20200416051211-89c76fbcd5d1 // indirect
	k8s.io/apimachinery v0.18.4
	k8s.io/client-go v0.18.4
	k8s.io/utils v0.0.0-20200603063816-c1c6865ac451 // indirect
)
